package api.service.repositories;

import api.service.models.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CustomerRepoTest {

    private CustomerRepo repo = new CustomerRepo();

    @Test
    public void testGetCustomer(){
        int id = 2;
        Customer customer = repo.getCustomer(id);
        assertEquals("Arnold Schwarzenegger", customer.getName());
        assertEquals("titanusstraat", customer.getAddress());
        assertEquals("6943 RC", customer.getZipcode());
        assertEquals("Geldrop", customer.getCity());
    }

}
