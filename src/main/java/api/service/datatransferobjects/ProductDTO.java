package api.service.datatransferobjects;

import api.service.models.Product;
import api.service.models.ProductCategory;
import api.service.repositories.ProductCategoryRepo;

import java.util.ArrayList;
import java.util.List;

public class ProductDTO {
    private int id;
    private String name;
    private int price;
    private ProductCategory category;
    private String productCode;

    public ProductDTO(int id, String name, float price, int categoryId, String productCode){
        ProductCategoryRepo category = new ProductCategoryRepo();

        this.id = id;
        this.name = name;
        this.price = (int)Math.round(price * 100.0);
        this.category = category.getCategory(categoryId);
        this.productCode = productCode;
    }

    public ProductDTO(Product product){
        ProductCategoryRepo category = new ProductCategoryRepo();

        this.id = product.getId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.category = category.getCategory(product.getCategoryId());
        this.productCode = product.getProductCode();
    }

    public ProductDTO(){

    }

    public List<ProductDTO> getProductList(List<Product> products){
        List<ProductDTO> productList = new ArrayList<>();
        for (Product product : products)
        {
            productList.add( new ProductDTO(product) );
        }

        return productList;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String Name){
        this.name = name;
    }

    public int getPrice(){
        return price;
    }

    public void setPrice(int price){
        this.price = price;
    }

    public ProductCategory getCategory(){
        return category;
    }

    public void setCategory(ProductCategory category){
        this.category = category;
    }

    public String getProductCode(){
        return productCode;
    }

    public void setProductCode(String productCode){
        this.productCode = productCode;
    }
}
