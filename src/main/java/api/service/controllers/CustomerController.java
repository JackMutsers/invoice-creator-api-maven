package api.service.controllers;

import api.service.models.Customer;
import api.service.repositories.CustomerRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/customers")
public class CustomerController {

    private CustomerRepo repo = new CustomerRepo();

    @GetMapping(path="/{id}")
    public @ResponseBody ResponseEntity<Customer> getCustomer(@PathVariable int id) {
        Customer customer = repo.getCustomer(id);
        if (customer == null) {
            return new ResponseEntity("Please provide a valid customer identifier.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Customer>(customer, HttpStatus.FOUND);
        }
    }

    @GetMapping(path="")
    public  @ResponseBody ResponseEntity<Iterable<Customer>> getAllCustomers() {
        List<Customer> customers = repo.getCustomers();
        if(customers.size() == 0){ return new ResponseEntity("There are currently no customers availible", HttpStatus.NOT_FOUND); }

        return new ResponseEntity<Iterable<Customer>>(customers, HttpStatus.FOUND);
    }

    @DeleteMapping(path = "/{Id}")
    public @ResponseBody ResponseEntity deleteCustomer(@RequestParam int id) {
        boolean success = repo.delete(id);
        // Idempotent method. Always return the same response (even if the resource has already been deleted before).
        if(success){
            return new ResponseEntity("Customer has been deleted successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity("customer not found.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path="")
    public @ResponseBody ResponseEntity<Customer> createCustomer(Customer customer) {
        int identifier = repo.add(customer);
        if (identifier == 0){
            return new ResponseEntity("The user can not be added because it is not complete", HttpStatus.CONFLICT);
        } else {
            Customer customer1 = repo.getCustomer(identifier);
            return new ResponseEntity<Customer>(customer1, HttpStatus.CREATED);
        }
    }

    @PutMapping(path ="/{id}")
    public @ResponseBody ResponseEntity updateCustomer(@PathVariable int id, Customer customer) {
        boolean result = repo.update(id, customer);
        if (!result){
            return new ResponseEntity("Please provide a valid Customer.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("Customer has successfully been updated.", HttpStatus.OK);
    }
}
