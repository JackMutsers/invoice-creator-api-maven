package api.service.controllers;

import api.service.datatransferobjects.ProductDTO;
import api.service.models.Customer;
import api.service.models.Product;
import api.service.models.User;
import api.service.repositories.UserRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    // this has to be static because the service is stateless:
    private UserRepo repo = new UserRepo();

    @GetMapping(path="/{id}")
    public @ResponseBody ResponseEntity<User> getUser(@PathVariable int id) {
        User user = repo.getUser(id);
        if (user == null) {
            return new ResponseEntity("Please provide a valid user identifier.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<User>(user, HttpStatus.FOUND);
        }
    }

    @GetMapping(path="")
    public @ResponseBody ResponseEntity<Iterable<User>> getAllUser() {
        List<User> users = repo.getUsers();
        if(users.size() == 0){ return new ResponseEntity("There are currently no users availible", HttpStatus.NOT_FOUND); }

        return new ResponseEntity<Iterable<User>>(users, HttpStatus.FOUND);
    }

    @DeleteMapping(path = "/{Id}")
    public @ResponseBody ResponseEntity deleteUser(@PathVariable int id) {
        boolean success = repo.delete(id);
        if(success){
            return new ResponseEntity("User has been deleted successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity("User was not found.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path="")
    public @ResponseBody ResponseEntity<User> createUser(User user) {
        boolean success = repo.add(user);
        if (!success){
            return new ResponseEntity("The user can not be added", HttpStatus.CONFLICT);
        } else {
            User user1 = repo.getUser(user.getId());
            return new ResponseEntity<User>(user1, HttpStatus.CREATED);
        }
    }

    @PutMapping(path ="/{id}")
    public @ResponseBody ResponseEntity updateUser(@PathVariable int id, User user) {
        boolean result = repo.update(id, user);
        if (!result){
            return new ResponseEntity("Please provide a valid user.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("User has successfully been updated.", HttpStatus.OK);

    }
}
