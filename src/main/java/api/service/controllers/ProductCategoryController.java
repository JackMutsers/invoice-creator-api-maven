package api.service.controllers;

import api.service.models.Customer;
import api.service.models.ProductCategory;
import api.service.repositories.ProductCategoryRepo;
import jdk.jfr.Category;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/productcategory")
public class ProductCategoryController {

    // this has to be static because the service is stateless:
    private ProductCategoryRepo repo = new ProductCategoryRepo();

    @GetMapping(path="/{id}")
    public @ResponseBody ResponseEntity<ProductCategory> getCategory(@PathVariable int id) {
        ProductCategory category = repo.getCategory(id);
        if (category == null) {
            return new ResponseEntity("Please provide a valid category identifier.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<ProductCategory>(category, HttpStatus.FOUND);
        }
    }

    @GetMapping(path="")
    public @ResponseBody ResponseEntity<Iterable<ProductCategory>> getAllCategory() {
        List<ProductCategory> categories = repo.getCategories();
        if(categories.size() == 0){ return new ResponseEntity("There are currently no customers availible", HttpStatus.NOT_FOUND); }
        return new ResponseEntity<Iterable<ProductCategory>>(categories, HttpStatus.FOUND);
    }

    @DeleteMapping(path = "/{Id}")
    public @ResponseBody ResponseEntity deleteCategory(@PathVariable int id) {
        boolean success = repo.delete(id);
        if(success){
            return new ResponseEntity("Product category has been deleted successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity("Product category was not found.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path="")
    public @ResponseBody ResponseEntity<ProductCategory> createCategory(ProductCategory category) {
        boolean success = repo.add(category);
        if (!success){
            return new ResponseEntity("The user can not be added because it is not complete", HttpStatus.CONFLICT);
        } else {
            ProductCategory productCategory = repo.getCategory(category.getId());
            return new ResponseEntity<ProductCategory>(productCategory, HttpStatus.CREATED);
        }
    }

    @PutMapping(path ="/{id}")
    public @ResponseBody ResponseEntity updateCategory(@PathVariable int id, ProductCategory category) {
        boolean result = repo.update(id, category);
        if (!result){
            return new ResponseEntity("Please provide a valid product category.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("Product category has successfully been updated.", HttpStatus.OK);

    }
}
