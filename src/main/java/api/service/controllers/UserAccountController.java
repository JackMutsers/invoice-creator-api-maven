package api.service.controllers;

import api.service.datatransferobjects.ProductDTO;
import api.service.datatransferobjects.UserAccountDTO;
import api.service.models.Product;
import api.service.models.UserAccount;
import api.service.repositories.UserAccountRepo;
import api.service.repositories.UserRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/useraccount")
public class UserAccountController {

    // this has to be static because the service is stateless:
    private UserAccountRepo accountRepo = new UserAccountRepo();
    private UserRepo userRepo = new UserRepo();

    @PostMapping(path="/login")
    public @ResponseBody ResponseEntity<UserAccountDTO> login(UserAccount account) {
        try{
            UserAccountDTO user = accountRepo.getByLogin(account.getUsername(), account.getPassword());
            if (user == null) {
                return new ResponseEntity("Login credentials were incorrect!", HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<UserAccountDTO>(user, HttpStatus.FOUND);
            }
        }
        catch (Exception ex){
            return new ResponseEntity("Login credentials were incorrect!", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping(path = "/{Id}")
    public @ResponseBody ResponseEntity deleteUser(@PathVariable int id, UserAccount account) {
        try{
            UserAccountDTO userAccount = accountRepo.getByLogin(account.getUsername(), account.getPassword());
            if (userAccount == null) {
                return new ResponseEntity("The user you are trying to delete does not exist.", HttpStatus.BAD_REQUEST);
            } else {
                if (userAccount.getId() == id) {
                    accountRepo.delete(id);
                    userRepo.delete(userAccount.getUser().getId());

                    return new ResponseEntity("Account has been deleted successfully.", HttpStatus.OK);
                } else {
                    return new ResponseEntity("Please login with the account you are trying to delete.", HttpStatus.BAD_REQUEST);
                }
            }
        }
        catch (Exception ex){
            return new ResponseEntity("Please login with the account you are trying to delete.", HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path="")
    public @ResponseBody ResponseEntity<UserAccountDTO> createUser(UserAccount user) throws Exception {
        boolean success = accountRepo.add(user);
        if (!success){
            return new ResponseEntity("The account can not be created", HttpStatus.CONFLICT);
        } else {
            UserAccount userAccount = accountRepo.getUserAccount(user.getId());
            UserAccountDTO accountDTO = new UserAccountDTO(userAccount);
            return new ResponseEntity<UserAccountDTO>(accountDTO, HttpStatus.CREATED);
        }
    }

    @PutMapping(path ="/{id}")
    public @ResponseBody ResponseEntity updateUser(@PathVariable int id, UserAccount account) {
        boolean result = accountRepo.update(id, account);
        if (!result){
            return new ResponseEntity("Please provide valid account details.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("Product has successfully been updated.", HttpStatus.OK);
    }
}
