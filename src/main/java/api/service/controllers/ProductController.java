package api.service.controllers;

import api.service.datatransferobjects.ProductDTO;
import api.service.datatransferobjects.UserAccountDTO;
import api.service.models.Customer;
import api.service.models.Product;
import api.service.repositories.ProductRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductController {

    // this has to be static because the service is stateless:
    private ProductRepo repo = new ProductRepo();

    @GetMapping(path="/{id}")
    public @ResponseBody ResponseEntity<ProductDTO> getProduct(@PathVariable int id) {
        ProductDTO product = new ProductDTO(repo.getProduct(id));
        if (product == null) {
            return new ResponseEntity("Please provide a valid product identifier.", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<ProductDTO>(product, HttpStatus.FOUND);
        }
    }

    @GetMapping(path="")
    public @ResponseBody ResponseEntity<Iterable<ProductDTO>> getAllProducts() {
        ProductDTO Dto = new ProductDTO();
        List<ProductDTO> products = Dto.getProductList(repo.getProducts());
        if(products.size() == 0){ return new ResponseEntity("There are currently no products availible", HttpStatus.NOT_FOUND); }

        return new ResponseEntity<Iterable<ProductDTO>>(products, HttpStatus.FOUND);
    }

    @DeleteMapping(path = "/{Id}")
    public @ResponseBody ResponseEntity deleteProduct(@PathVariable int id) {
        boolean success = repo.delete(id);
        if(success){
            return new ResponseEntity("Product has been deleted successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity("Product not found.", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path="")
    public @ResponseBody ResponseEntity<ProductDTO> createProduct(Product product) {
        boolean success = repo.add(product);
        if (!success){
            return new ResponseEntity("The product can not be added", HttpStatus.CONFLICT);
        } else {
            Product product1 = repo.getProduct(product.getId());
            ProductDTO productDTO = new ProductDTO(product1);
            return new ResponseEntity<ProductDTO>(productDTO, HttpStatus.CREATED);
        }
    }

    @PutMapping(path ="/{id}")
    public @ResponseBody ResponseEntity updateProduct(@PathVariable int id, Product product) {
        boolean result = repo.update(id, product);
        if (!result){
            return new ResponseEntity("Please provide a valid product.", HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity("Product has successfully been updated.", HttpStatus.OK);

    }
}
